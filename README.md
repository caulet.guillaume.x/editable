# EDITABLE

---

## Documentation du composant

---

## Table des Matières

---

1. [Infos-Générales]
2. [Technologies]
3. [Installation]
4. [Fonctionnalitées]
5. [Contact]

---

---

---

### 1. [Infos-Générales]

---

**Editable** est un tableau éditable qui permet de modifier un ensemble d’objets homogènes fournis dans un tableau en JSON.
Il permet d'afficher un ou plusieurs tableau JSON, le/les modifer, et le/les sauvegarder.

---

---

### 2. [Technologies]

---

Pour cette application, on utilise pricipalement du **JavaScript**, qui va géréner la structure **HTML** d'un tableau en fonction d'un/des fichiers **JSON**, du **PHP** pour sauvegarder les modifications éventuelles,et enfin du **CSS** pour la mise en forme.

---

---

### 3. [Installation]

---

> Récupération du Dépot Git

---

> - Ouvrir le lien du projet sur [**Git Lab**](https://gitlab.com/caulet.guillaume.x/editable)
> - Cloner le projet sur votre serveur (LAMP/WAMP/...) voir [**Cloner sur Git Hub**](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
> - Insérer votre/vos tableaux JSON dans le fichier _DATA_
> - Modifier ensuite le chemin d'accès du fichier dans la page _index.html_

```html
<body>
  <div id="fichier" data-url="../data/NOM-DE-VOTRE-FICHIER-1.json"></div>
</body>
```

> - Puis rendez vous sur la page _editable.js_ dans la section _Création Bouton Affichage Fichier_, rentrez le **même nom de fichier** que dans la page _test.html_

```js
let btn1 = document.createElement("button");
btn1.textContent = "NOM-DE-VOTRE-FICHIER-1";
btn1.onclick = function () {
  fichier.setAttribute("data-url", "../data/NOM-DE-VOTRE-FICHIER-1.json");
  init();
};
```

> - Si vous avez d'autres tableaux à générer, rendez vous sur la page _editable.js_ dans la section _Création Bouton Affichage Fichier_, remplacez le nom du fichier du second bouton ainsi que le nom du second bouton celui de votre second fichier

```js
let btn2 = document.createElement("button");
btn2.textContent = "NOM-DE-VOTRE-FICHIER-2";
btn2.onclick = function () {
  fichier.setAttribute("data-url", "../data/NOM-DE-VOTRE-FICHIER-2.json");
  init();
};
```

> - Faites la même chose si il y a une troisième fichier à la suite, dans **btn3**
> - Si vous avez plus de trois tableau JSON à afficher, copier le code de création du bouton à la suite en modifiant le numéro du bouton (btn?) puis repétez la même opération que ci-dessus pour le nom de vos fichiers

```js
let btn? = document.createElement("button");
  btn?.textContent = "NOM-DE-VOTRE-FICHIER-?";
  btn?.onclick = function () {
    fichier.setAttribute("data-url", "../data/NOM-DE-VOTRE-FICHIER-?.json");
    init();
  };
```

> - **ATTENTION A L'INDENTATION POUR L'AJOUT DE BOUTON**

---

---

### 4. [Fonctionnalitées]

---

> - Affichage des données d'un tableau qui se trouve dans un fichier JSON.
> - Filtre de donnée.
> - Tri des données sur chaque colonne (ordre alphanumérique).
> - Modification des données du tableau.
> - ~Ajout de ligne / création de nouvel objet.~
> - ~Sauvegarde des modifications.~

---

---

### 5. [Contact]

---

> - Pour toutes demandes, problèmes, contactez moi via mon [adresse mail](caulet.guillaume.x@gmail.com)
