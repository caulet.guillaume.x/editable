// Récupération de l'URL

let myArray;
function init() {
  let url = fichier.getAttribute("data-url");

  // Récupération des données

  fetch(url)
    .then((response) => response.json())
    .then((arr) => {
      myArray = arr;

      // Appel Fonction Affichage Tableau

      displayTab(myArray);
    })

    // Affichage erreur HTTP

    .catch((err) => {
      console.log(err);
    });
}
// Fonction Affichage Tableau

function displayTab() {
  let html = document.querySelector("#fichier");
  html.innerHTML = "";

  //Création Bouton Affichage Fichier

  let btn1 = document.createElement("button");
  btn1.textContent = "person.json";
  btn1.onclick = function () {
    fichier.setAttribute("data-url", "../data/person.json");
    init();
  };

  let btn2 = document.createElement("button");
  btn2.textContent = "product.json";
  btn2.onclick = function () {
    fichier.setAttribute("data-url", "../data/product.json");
    init();
  };

  let btn3 = document.createElement("button");
  btn3.textContent = "points.json";
  btn3.onclick = function () {
    fichier.setAttribute("data-url", "../data/points.json");
    init();
  };

  html.appendChild(btn1);
  html.appendChild(btn2);
  html.appendChild(btn3);

  // Filtre

  let filtre = document.createElement("input");
  filtre.setAttribute("type", "search");
  filtre.placeholder = "Filtre";
  filtre.id = "fltr";

  html.appendChild(filtre);

  document.querySelector("#fltr").addEventListener("keyup", function () {
    const that = this;
    const elements = Array.from(document.querySelectorAll(".lgn"));
    elements.forEach((lgn) => {
      lgn.style.display = null;
    });
    elements
      .filter(
        (lgn) => !lgn.innerText.toLowerCase().includes(that.value.toLowerCase())
      )
      .forEach((lgn) => (lgn.style.display = "none"));
  });

  // Création Tableau

  let tabDiv = document.createElement("table");
  tabDiv.classList.add("arr");
  let thead = document.createElement("thead");
  thead.classList.add("thd");
  let tbody = document.createElement("tbody");
  tbody.classList.add("tby");

  for (let i in myArray[0]) {
    let key = document.createElement("th");
    key.setAttribute("class", "key");
    key.id = i;
    key.innerText = i;
    thead.appendChild(key);
  }
  for (let i in myArray) {
    let lgn = document.createElement("tr");
    lgn.classList.add("lgn");
    lgn.contentEditable = "true";
    tbody.appendChild(lgn);
    for (let j in myArray[i]) {
      let data = document.createElement("td");
      data.classList.add("data");
      data.setAttribute("key", j);
      data.innerText = myArray[i][j];
      lgn.appendChild(data);
    }
  }
  html.appendChild(tabDiv);
  tabDiv.appendChild(thead);
  tabDiv.appendChild(tbody);

  // Ajouter une ligne

  let add = document.createElement("button");
  add.textContent = "+";
  html.appendChild(add);
  add.onclick = function () {
    lgn = document.createElement("tr");
    lgn.classList.add("lgn");
    lgn.contentEditable = "true";
    tbody.appendChild(lgn);

    for (let e in myArray[0]) {
      let data = document.createElement("td");
      data.classList.add("data");
      data.innerText = "";
      lgn.appendChild(data);
    }
  };

  // Classer

  let arrK = document.querySelectorAll(".key");
  let bool = 1;
  for (let k of arrK) {
    k.addEventListener("click", function (event) {
      const allTr = Array.from(document.querySelectorAll(".lgn"));
      let key = event.target.id;

      if (bool === 0) {
        allTr.sort(function (a, b) {
          let pra = a.querySelector("[key='" + key + "']").innerText;
          let prb = b.querySelector("[key='" + key + "']").innerText;
          bool = 1;
          return pra.localeCompare(prb);
        });
      } else {
        allTr.sort(function (a, b) {
          let pra = a.querySelector("[key='" + key + "']").innerText;
          let prb = b.querySelector("[key='" + key + "']").innerText;
          bool = 0;
          return prb.localeCompare(pra);
        });
      }

      for (let tr of allTr) {
        tabDiv.appendChild(tr);
      }
    });
  }

  // Sauvegarder

  let savBtn = document.createElement("button");
  savBtn.textContent = "Sauvegarder";
  html.appendChild(savBtn);
}
init();
